<?xml version="1.0" encoding="UTF-8"?>
<project name="magento">

    <import file="lib.sysinfo.xml" />
    <import file="build.composer.xml" />

    <taskdef resource="net/sf/antcontrib/antlib.xml" />

    <!--
    ### start the build process
    -->
    <target name="build" depends="load-env,generate-version" description="start the build process">
        <echo message="Deploying to ${deployment-env}" />

        <antcall target="call-target-for-servers">
            <param name="targets" value="check-version"/>
        </antcall>

        <antcall target="build-job" />
    </target>

    <!--
    ### load environment settings
    -->
    <target name="load-env">
        <condition property="env-exists">
            <or>
                <equals arg1="${deployment-env}" arg2="live"/>
                <equals arg1="${deployment-env}" arg2="stage"/>
                <equals arg1="${deployment-env}" arg2="dev"/>
                <equals arg1="${deployment-env}" arg2="vagrant"/>
            </or>
        </condition>

        <fail unless="env-exists" message="Unknown building environment" />
        <property file="build.properties/${deployment-env}.build.properties" />
    </target>

    <!--
    ### generate version number

        currently there are two types supported:

        - manually specified by environment variable: [env].version.properties
        - automatically created depending on current timestamp and git revision
    -->
    <target name="generate-version">
        <if>
            <and>
                <equals arg1="${project.versiontype}" arg2="file" />
                <available file="build.properties/${deployment-env}.version.properties"/>
            </and>
            <then>
                <var file="build.properties/${deployment-env}.version.properties" />
            </then>
            <elseif>
                <equals arg1="${project.versiontype}" arg2="tag" />
                <then>
                    <exec executable="git" outputproperty="project.version" failifexecutionfails="false" errorproperty="">
                        <arg value="describe"/>
                        <arg value="--tags"/>
                        <arg value="--abbrev=0"/>
                    </exec>
                </then>
            </elseif>
            <else>
                <tstamp>
                    <format property="timestamp" pattern="yyyyMMdd_HHmmss" locale="de,DE"/>
                </tstamp>

                <exec executable="git" outputproperty="git.revision" failifexecutionfails="false" errorproperty="">
                    <arg value="describe"/>
                    <arg value="--tags"/>
                    <arg value="--always"/>
                    <arg value="HEAD"/>
                </exec>
                <condition property="repository.version" value="${git.revision}" else="unknown">
                    <and>
                        <isset property="git.revision"/>
                        <length string="${git.revision}" trim="yes" length="0" when="greater"/>
                    </and>
                </condition>
                <property name="project.version" value="${timestamp}_${git.revision}"/>
            </else>
        </if>
    </target>

    <!--
    ### run all for the build process necessary tasks and all target server based tasks for each one
    -->
    <target name="build-job" depends="build-before,prepare,sysinfo.which-php,add-mage-core,composer.self-update">

        <if>
            <or>
                <equals arg1="${deployment-env}" arg2="vagrant"/>
                <equals arg1="${deployment-env}" arg2="dev"/>
                <equals arg1="${deployment-env}" arg2="stage"/>
            </or>
            <then>
                <antcall target="composer.install-dev" />
            </then>
            <else>
                <antcall target="composer.install" />
            </else>
        </if>

        <antcall target="add-project-files" />
        <antcall target="phplint" />

        <antcall target="call-target-for-servers">
            <param name="targets" value="deploy-before,deploy,deploy-after,switch-version,switch-version-after,import-dump,update-settings,cleanup-versions"/>
        </antcall>
        <antcall target="build-after"/>
        <echo message="Done!" />
    </target>

    <!--
    ### Utility Task to run specified targets for each server
    -->
    <target name="call-target-for-servers">
        <for list="${targets}" param="targetName">
            <sequential>
                <for list="1,2,3,4,5" param="servernr">
                    <sequential>
                        <if>
                            <isset property="server@{servernr}.host"/>
                            <then>
                                <antcall target="message">
                                    <param name="text" value="Calling target '@{targetName}' for server@{servernr}"/>
                                </antcall>
                                <for list="${project.server-environment-variables}" param="paramname">
                                    <sequential>
                                        <var name="server.@{paramname}" unset="true"/>
                                        <property name="server.@{paramname}" value="${server@{servernr}.@{paramname}}"/>
                                        <if>
                                            <matches string="${server.@{paramname}}" pattern="^\$\{(.*)\}$"/>
                                            <then>
                                                <var name="server.@{paramname}" unset="true"/>
                                            </then>
                                        </if>
                                    </sequential>
                                </for>
                                <antcall target="@{targetName}" />
                            </then>
                        </if>
                    </sequential>
                </for>
            </sequential>
        </for>
    </target>

    <!--
    ### check if the current version already exits
    -->
    <target name="check-version">
        <echo message="Checking if version ${project.version} exists" />

        <antcall target="exec-ssh-command">
            <param name="ssh-cmd" value="mkdir ${server.versions-path}${project.version} &amp;&amp; rmdir ${server.versions-path}${project.version}" />
        </antcall>
    </target>

    <!--
    ### prepare current working copy for deployment
    -->
    <target name="prepare">
        <echo message="Removing un-tracked file from GIT repo" />
        <exec executable="git">
            <arg value="clean" />
            <arg value="-d" />
            <arg value="-f" />
            <arg value="-q" />
        </exec>

        <echo message="Create .build folder" />
        <exec executable="mkdir">
            <arg value=".build" />
        </exec>
    </target>

    <!--
    ### get Magento core files
    -->
    <target name="add-mage-core">
        <echo message="Download magento-${project.magento-version}.tar.gz" />
        <exec executable="wget" failonerror="true">
            <arg line="-nv --no-check-certificate --http-user=flagbit --http-password=ahxophul8nah5pho6Ohng1chee5joi -P ${basedir}/ https://ci.flagbit.com/magento/magento-${project.magento-version}.tar.gz"/>
        </exec>
        <echo message="Extracting magento-${project.magento-version}.tar.gz to .build" />
        <exec executable="tar" failonerror="true">
            <arg value="-C" />
            <arg value="${basedir}/.build" />
            <arg value="-zxf" />
            <arg value="${basedir}/magento-${project.magento-version}.tar.gz" />
            <arg value="--strip=1" />
        </exec>
    </target>

    <!--
    ### add project files
    -->
    <target name="add-project-files">
        <echo message="Copy project file to .build folder" />
        <copy todir="${basedir}/.build" force="true" overwrite="true">
            <fileset dir="${basedir}/src/" />
        </copy>
    </target>

    <!--
    ### compile stylesheets
    -->
    <target name="compile-style">
        <exec executable="bundle" failonerror="true">
            <arg value="install" />
        </exec>
        <exec executable="bundle" failonerror="true">
            <arg value="exec" />
            <arg value="ruby" />
            <arg value="compile.rb" />
        </exec>
    </target>

    <!--
    ### Frontend Build System on NPM, Bower and Grunt
    -->
    <target name="alternative-frontend-build">
      <if>
        <available file="package.json"/>
        <then>
          <exec executable="npm" failonerror="true">
            <arg value="install" />
          </exec>
        </then>
      </if>
      <if>
        <and>
          <available file=".bowerrc"/>
          <available file="bower.json"/>
        </and>
        <then>
          <exec executable="./node_modules/.bin/bower" failonerror="true">
            <arg value="install" />
          </exec>
        </then>
      </if>
      <if>
        <or>
          <available file="Gruntfile.coffee"/>
          <available file="Gruntfile.js"/>
        </or>
        <then>
          <exec executable="./node_modules/.bin/grunt" failonerror="true">
            <arg value="default" />
          </exec>
        </then>
      </if>
    </target>

    <!--
    ### pack the whole project as tar archive
    -->
    <target name="package">
        <echo message="Packing project's files" />
        <exec executable="tar" failonerror="true">
            <arg value="--exclude" />
            <arg value=".build/.idea*" />
            <arg value="--exclude" />
            <arg value="*.DS_Store*" />
            <arg value="--exclude" />
            <arg value=".build/.htaccess" />
            <arg value="-zcf" />
            <arg value="${project.version}.tar.gz" />
            <arg value=".build" />
        </exec>
    </target>

    <!--
    ### Utility to execute shell commands on the server
    -->
    <target name="exec-ssh-command">
        <sshexec host="${server.host}" port="${server.port}" username="${server.user}" keyfile="${server.key-file}" passphrase="${server.passphrase}" trust="yes"
                 command="${ssh-cmd}" />
    </target>

    <!--
    ### run phplint
    -->
    <target name="phplint">

        <if>
            <equals arg1="${project.phplint}" arg2="parallel"/>
            <then>
                <exec executable="${basedir}/vendor/bin/parallel-lint" failonerror="true">
                    <arg line="--exclude" />
                    <arg path="${basedir}/vendor/" />
                    <arg line="--exclude" />
                    <arg path="${basedir}/puphpet/" />
                    <arg line="-e" />
                    <arg path="phtml,php" />
                    <arg path="${basedir}/src/" />
                </exec>
            </then>
            <else>
                <apply executable="php" failonerror="true">
                    <fileset dir="${basedir}/src">
                        <include name="**/*.php"/>
                        <include name="**/*.phtml"/>
                    </fileset>
                    <arg line="-l"/>
                </apply>
            </else>
        </if>
    </target>

    <!--
    ### clean the magento cache
    -->
    <target name="n98-clean-cache">
        <antcall target="mage-run-cmd">
            <param name="magerun-cmd" value="cache:flush" />
        </antcall>
    </target>

    <!--
    ### run a magerun command
    -->
    <target name="mage-run-cmd">
        <antcall target="exec-ssh-command">
            <param name="ssh-cmd" value="cd ${server.versions-path}${project.version} ; ${server.mage-run-path} ${magerun-cmd}" />
        </antcall>
    </target>

    <!--
    ### remove old versions

        is only enabled when server.keepversions is set
        Note: when you like to keep the last 5 versions you have to set the value to 6
    -->
    <target name="cleanup-versions">
        <if>
            <isset property="server.keepversions"/>
            <then>
                <antcall target="exec-ssh-command">
                    <param name="ssh-cmd" value="ls -t -1 ${server.versions-path} | tail -n +${server.keepversions} | xargs -I {} rm -rf ${server.versions-path}{}" />
                </antcall>
            </then>
        </if>

    </target>

    <!--
    ### switches the symlink of the document root which makes the working copy to live version
    -->
    <target name="switch-version">
        <antcall target="exec-ssh-command">
            <param name="ssh-cmd" value="ln -sfn ${server.versions-path}${project.version} ${server.apache-dir}" />
        </antcall>
    </target>

    <!--
    ### imports a specified database dump
    -->
    <target name="import-dump" depends="load-env">
        <if>
            <isset property="server.import_database_dump"/>
            <then>
                <antcall target="mage-run-cmd">
                    <param name="magerun-cmd" value="db:import ${server.import_database_dump}" />
                </antcall>
            </then>
            <else>
                <echo message="No dumps were imported." />
            </else>
        </if>
    </target>

    <!--
    ### runs the deployment process
    -->
    <target name="deploy" depends="compile-style,package">
        <scp
                file="${project.version}.tar.gz" todir="${server.user}@${server.host}:/tmp"
                keyfile="${server.key-file}" passphrase="${server.passphrase}" trust="yes" sftp="true" port="${server.port}"
                />

        <antcall target="exec-ssh-command">
            <param name="ssh-cmd" value="mkdir ${server.versions-path}${project.version}" />
        </antcall>

        <antcall target="exec-ssh-command">
            <param name="ssh-cmd" value="tar -C ${server.versions-path}${project.version} -zxf /tmp/${project.version}.tar.gz --strip=1 &amp;&amp; find ${server.versions-path}${project.version} -name &quot;._*&quot; -delete" />
        </antcall>

        <antcall target="exec-ssh-command">
            <param name="ssh-cmd" value="cp ${server.apache-dir}/app/etc/local.xml ${server.versions-path}${project.version}/app/etc/local.xml" />
        </antcall>

        <antcall target="exec-ssh-command">
            <param name="ssh-cmd" value="cp ${server.apache-dir}/.htaccess ${server.versions-path}${project.version}/.htaccess" />
        </antcall>

        <antcall target="exec-ssh-command">
            <param name="ssh-cmd" value="rm -r ${server.versions-path}${project.version}/media ; ln -s ${server.deployment-path}commons/media ${server.versions-path}${project.version}/" />
        </antcall>

        <antcall target="exec-ssh-command">
            <param name="ssh-cmd" value="echo -e '\ndeny from all' >> ${server.versions-path}${project.version}/downloader/.htaccess" />
        </antcall>
    </target>

    <!--
    ### update magento settings which ca be specified by env variables:
        project.magento_setting[INT]
    -->
    <target name="update-settings">
        <propertyselector property="settings.list"
                          delimiter=","
                          match="project\.magento_setting_([0-9]+)"
                          select="\0"
                          casesensitive="false" />

        <if>
            <isset property="settings.list"/>
            <then>
                <for list="${settings.list}" param="paramname">
                    <sequential>
                        <echo message="core_config_data: ${@{paramname}}" />
                        <antcall target="mage-run-cmd">
                            <param name="magerun-cmd" value="config:set ${@{paramname}}" />
                        </antcall>
                    </sequential>
                </for>
            </then>
            <else>
                <echo message="No settings were updated." />
            </else>
        </if>
    </target>

    <!--
    ### Utility to print a message
    -->
    <target name="message">
        <echo message="      \/" />
        <echo message=" ___  _@@" />
        <echo message="(___)(_)        ${text}" />
        <echo message="//|| ||" />
    </target>

    <!--
    ### Utility methods which are only there to give you the possibility to extend some extra functionality
    -->
    <target name="build-before"/>
    <target name="build-after"/>
    <target name="deploy-before"/>
    <target name="deploy-after"/>
    <target name="switch-version-after"/>

</project>
