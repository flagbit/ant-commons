# General

## How to use

Add the submodule:

    git submodule add git@bitbucket.org:flagbit/ant-commons.git

Add this to your build.xml:

    <include file="ant-commons/build.composer.xml"/>

Now you can use the included targets:

    <target name="phpunit" depends="composer.install"/>


# Magento #

## How to extend the functionality?

There are a punch of helper targets which can be overwritten in your build.xml:
```
#!xml

    <!--
    ### Utility methods which are only there to give you the possibility to extend some extra functionality
    -->
    <target name="build-before"/>
    <target name="build-after"/>
    <target name="deploy-before"/>
    <target name="deploy-after"/>
    <target name="switch-version-after"/>
```



## use Database Import functionality

### 1. add a cronjob to generate a daily dump, Example: ###

```
#!bash

0 5 * * * cd /data/www/www.german-dream-nails.com/public && /data/www/bin/n98-magerun.phar db:dump --strip="@stripped" /data/www/www.german-dream-nails.com/magento_live.sql > /dev/null
```

### 2. specify the location of the dump to the ant deployment: ###
```
#!ant

server1.import_database_dump = /data/www/www.german-dream-nails.com/magento_live.sql
```

### 3. specify setup depending config data, example: ###
```
#!ant

project.magento_setting_1 = web/secure/base_url http://dev.german-dream-nails.com/
project.magento_setting_2 = web/unsecure/base_url http://dev.german-dream-nails.com/
project.magento_setting_3 = web/cookie/cookie_domain dev.german-dream-nails.com
```
